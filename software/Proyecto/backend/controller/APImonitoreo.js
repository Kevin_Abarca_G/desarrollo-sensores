var mysql = require('mysql');
var jwt = require('jsonwebtoken');

const secretkey = 'factorb202020';

exports.get_index = (req, res) => {
    res.send("Hola Mundo");
}
exports.create_db = (req, res) => {
    var con = mysql.createConnection({
        host: "localhost",
        user: "root",
        password: ""
    });

    con.connect(function(err) {
    if (err) throw err;
    console.log("Connected!");
    con.query("CREATE DATABASE factorb", function (err, result) {
        if (err) throw err;
        console.log("Base de datos factor ha sido creada!!");
    });
    });
    res.send("Base de datos factor ha sido creada!!");
}
exports.create_tb = (req, res) => {
    var con = mysql.createConnection({
        host: "localhost",
        user: "root",
        password: "",
        database: "factorb"
    });
    con.connect(function(err) {
    if (err) throw err;
    console.log("Connected!");
    var sql = "CREATE TABLE usuarios (id INT AUTO_INCREMENT PRIMARY KEY, nombre VARCHAR(255), apellidos VARCHAR(255), correo VARCHAR(255), usuario VARCHAR(255), rol VARCHAR(255), password VARCHAR(255), fecha_creacion TIMESTAMP DEFAULT CURRENT_TIMESTAMP)";
    con.query(sql, function (err, result) {
        if (err) throw err;
        console.log("Table created");
    });
    });
    res.send("creando tb");
}
exports.create_user = (req, res) => {
       const {nombre, apellidos, correo, usuario, rol, password} = req.body;
        var con = mysql.createConnection({
        host: "localhost",
        user: "root",
        password: "",
        database: "factorb"
        });
       con.connect(function(err) {
           console.log("Conectado a la base de datos!");

           var sql = `INSERT INTO usuarios (nombre, apellidos, correo, usuario, rol, password) VALUES ('${nombre}','${apellidos}','${correo}','${usuario}','${rol}','${password}')`;
           con.query(sql, function (err, result) {
             if (err) throw err;
             console.log("una fila insertada en factorb usuarios:");
           });
        });
        res.send("Usuario Creado...");
}
exports.remove_user = (req, res) => {
    const {id} = req.body;

    res.send("Usuario Eliminado...");
    var con = mysql.createConnection({
        host: "localhost",
        user: "root",
        password: "",
        database: "factorb"
    });
    con.connect(function(err) {
        if (err) throw err;
        var sql = `DELETE FROM usuarios WHERE id = '${id}'`;
        con.query(sql, function (err, result) {
          if (err) throw err;
          console.log("Number of records deleted: " + result.affectedRows);
        });
    });
}
exports.update_user = (req, res) => {
    const {id, nombre, apellidos, correo, usuario, rol, password} = req.body;
    var con = mysql.createConnection({
        host: "localhost",
        user: "root",
        password: "",
        database: "factorb"
    });
    con.connect(function(err) {
        if (err) throw err;
        var sql = `UPDATE usuarios SET  nombre = '${nombre}', apellidos = '${apellidos}', correo = '${correo}', usuario = '${usuario}', rol = '${rol}', password = '${password}' WHERE id = '${id}'`; 
        con.query(sql, function (err, result) {
          if (err) throw err;
          console.log(result.affectedRows + " record(s) updated");
        });
    });
    res.send("Usuario Actualizado...");
}
exports.loggin_user = (req, res) => {
    const {usuario, password} = req.body;

    //encontrar usuario
    var con = mysql.createConnection({
        host: "localhost",
        user: "root",
        password: "",
        database: "factorb"
    });
    con.connect(function(err) {
        if (err) throw err;
        console.log(req.body);
        con.query(`SELECT password FROM usuarios WHERE usuario = '${usuario}'`, function (err, result, fields) {
          if (err) throw err;
          if(result.length > 0){
              console.log("hay resultados");
              if(password == result[0].password){
                console.log("todo correcto iniciando sesion");
                const token = jwt.sign({_user: usuario}, secretkey);
                console.log(token);
                res.status(200).json({token});
              }
              else{
                console.log("password incorrecta");  
              }
          }
          else{
            console.log("no existe");
          }
        });
    });
    
}

exports.verify_token = (req, res, next) => {
    
    let token = req.headers.authorization.split(' ')[1];
    try {
        var payload = jwt.verify(token, secretkey);
        if (payload) {
            res.status(200).send("verificacion exitosa");
        }
        else{
            res.status(401).send('Unauhtorized Request');
        }
        next();  
      } catch(err) {
        res.status(401).send('Unauhtorized Request');
      }
    
    
    
    

    /*async function verifyToken(req, res, next) {
	try {
		if (!req.headers.authorization) {
			return res.status(401).send('Unauhtorized Request');
		}
		let token = req.headers.authorization.split(' ')[1];
		if (token === 'null') {
			return res.status(401).send('Unauhtorized Request');
		}

		const payload = await jwt.verify(token, 'secretkey');
		if (!payload) {
			return res.status(401).send('Unauhtorized Request');
		}
		req.userId = payload._id;
		next();
	} catch(e) {
		//console.log(e)
		return res.status(401).send('Unauhtorized Request');
	}
} */
    res.send("verificando token...");
} 


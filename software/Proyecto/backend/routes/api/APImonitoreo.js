var express = require('express');
var router = express.Router();
var controller = require('../../controller/APImonitoreo');


router.get('/', controller.get_index);
router.post('/createdb', controller.create_db);
router.post('/createtb', controller.create_tb);
router.post('/create', controller.create_user);
router.post('/remove', controller.remove_user);
router.post('/update', controller.update_user);
router.post('/loggin', controller.loggin_user);
router.post('/verifytoken', controller.verify_token);
module.exports = router;
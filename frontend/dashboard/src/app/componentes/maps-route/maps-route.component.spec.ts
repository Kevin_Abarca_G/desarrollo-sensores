import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MapsRouteComponent } from './maps-route.component';

describe('MapsRouteComponent', () => {
  let component: MapsRouteComponent;
  let fixture: ComponentFixture<MapsRouteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MapsRouteComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MapsRouteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DelnodeComponent } from './delnode.component';

describe('DelnodeComponent', () => {
  let component: DelnodeComponent;
  let fixture: ComponentFixture<DelnodeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DelnodeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DelnodeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

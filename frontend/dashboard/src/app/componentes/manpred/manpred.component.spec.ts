import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ManpredComponent } from './manpred.component';

describe('ManpredComponent', () => {
  let component: ManpredComponent;
  let fixture: ComponentFixture<ManpredComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ManpredComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ManpredComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

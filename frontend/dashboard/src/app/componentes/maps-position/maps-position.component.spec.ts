import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MapsPositionComponent } from './maps-position.component';

describe('MapsPositionComponent', () => {
  let component: MapsPositionComponent;
  let fixture: ComponentFixture<MapsPositionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MapsPositionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MapsPositionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

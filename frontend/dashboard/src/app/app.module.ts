import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { MatSliderModule } from '@angular/material/slider';




import { AppComponent } from './app.component';
import { NavbarComponent } from './componentes/navbar/navbar.component';
import { DashboardComponent } from './componentes/dashboard/dashboard.component';
import { GoogleMapsModule} from '@angular/google-maps';
import { ContenidoDashComponent } from './componentes/contenido-dash/contenido-dash.component';
import { AddnodeComponent } from './componentes/addnode/addnode.component';
import { DelnodeComponent } from './componentes/delnode/delnode.component';
import { MapsRouteComponent } from './componentes/maps-route/maps-route.component';
import { MapsPositionComponent } from './componentes/maps-position/maps-position.component';
import { SensoresComponent } from './componentes/sensores/sensores.component';
import { ManpredComponent } from './componentes/manpred/manpred.component';
import { GraficosComponent } from './componentes/graficos/graficos.component';
import { TableComponent } from './componentes/table/table.component';
import { MessageComponent } from './componentes/message/message.component';
import { AlertsComponent } from './componentes/alerts/alerts.component';
import { ReportesComponent } from './componentes/reportes/reportes.component';
import { ConfComponent } from './componentes/conf/conf.component'; 
@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    DashboardComponent,
    ContenidoDashComponent,
    AddnodeComponent,
    DelnodeComponent,
    MapsRouteComponent,
    MapsPositionComponent,
    SensoresComponent,
    ManpredComponent,
    GraficosComponent,
    TableComponent,
    MessageComponent,
    AlertsComponent,
    ReportesComponent,
    ConfComponent
  ],
  imports: [
    BrowserModule,
    GoogleMapsModule,
    MatSliderModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
